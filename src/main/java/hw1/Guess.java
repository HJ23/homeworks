package hw1;

import java.util.Scanner;
import java.util.Random;

public class Guess {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Random rand = new Random();
        String name;
        System.out.println("yout name :");
        name = scan.next();
        System.out.println("let the game begin!");
        int random = rand.nextInt(101);
        int userVar;
        while (true) {
            System.out.println("Enter number :");
            userVar = scan.nextInt();
            if (userVar < random)
                System.out.println("Your number is too small.Please try again.");
            else if (userVar > random)
                System.out.println("Your number is too big.Please, try again.");
            else {
                System.out.println("Congratulations " + name);
                break;
            }
        }
    }
}
