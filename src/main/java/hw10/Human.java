package hw10;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Human {
    private String name;
    private String surname;
    private long birthDate = 0L;
    private int iq = 0;

    public void describeAge() {
        long diff = System.currentTimeMillis();
        diff = (diff - birthDate);
        long diffSeconds = diff / 1000 % 60;
        long diffMinutes = diff / (60 * 1000) % 60;
        long diffHours = diff / (60 * 60 * 1000) % 24;
        long diffDays = diff / (24 * 60 * 60 * 1000);

        System.out.print(diffDays + " days, ");
        System.out.print(diffHours + " hours, ");
        System.out.print(diffMinutes + " minutes, ");
        System.out.println(diffSeconds + " seconds.");
    }

    public Human() {
    }

    private long stringMilliSecondConverter(String time) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        Date date = formatter.parse(time);
        long milliSecond = date.getTime();
        return milliSecond;
    }

    public Human(String name, String surname, String birth, int iq) throws ParseException {
        this.name = name;
        this.surname = surname;
        this.birthDate = stringMilliSecondConverter(birth);
        this.iq = iq;
    }

    public Human(String name, String surname, long date) {
        this.name = name;
        this.surname = surname;
        this.birthDate = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String temp) {
        name = temp;
    }

    public String getSurname() {
        return name;
    }

    public void setSurname(String temp) {
        name = temp;
    }

    @Override
    public String toString() {
        SimpleDateFormat simple = new SimpleDateFormat("dd/MM/yyyy");
        Date dateBirthdate = new Date(birthDate);
        String birthDate = simple.format(dateBirthdate);
        printMe(("Human{name=" + name + ", surname=" + surname + ", year=" + birthDate + ", iq=" + iq));
        return ("Human{name=" + name + ", surname=" + surname + ", year=" + birthDate + ", iq=" + iq);
    }

    public static void printMe(String temp) {
        System.out.println(temp);
    }
}
