package hw11.collectionfamily;

import hw11.classes.Family;
import hw11.interfaces.FamilyDao;

import java.util.ArrayList;
import java.util.List;

public class CollectionFamilyDAO implements FamilyDao {
    private List<Family> families = new ArrayList<>();

    @Override
    public List<Family> getAllFamilies() {
        return families;
    }

    @Override
    public Family getFamilyByIndex(int index) {
        if (index > families.size())
            return null;
        return families.get(index);
    }

    @Override
    public boolean deleteFamily(int index) {
        if (index > families.size())
            return false;
        families.remove(index);
        return true;
    }

    @Override
    public void saveFamily(Family fam) {
        if (families.contains(fam))
            families.set(families.indexOf(fam), fam);
        else
            families.add(fam);
    }
}
