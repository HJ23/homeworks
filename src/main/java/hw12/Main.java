package hw12;

import hw12.classes.*;
import hw12.controller.FamilyController;

import java.text.ParseException;
import java.util.*;

public class Main {

    public static void main(String[] args) throws ParseException {

        Map<String, String> schedule = new HashMap<>();
        schedule.put(DayofWeek.MONDAY.name(), "Monday: get to work");
        schedule.put(DayofWeek.TUESDAY.name(), "Tuesday: meeting in the park");
        schedule.put(DayofWeek.WEDNESDAY.name(), "Wednesday: walk with dog");
        schedule.put(DayofWeek.THURSDAY.name(), "Thursday: make bread with josh");

        Set<String> habitsDog = new HashSet<String>();
        habitsDog.add("sleeping");
        habitsDog.add("playing");
        habitsDog.add("eating");
        habitsDog.add("running");

        Pet dogFamily11 = new Pet("doggie 1", Species.DOG, 4, 70, habitsDog);
        Pet dogFamily12 = new Pet("doggie 2", Species.DOG, 7, 70, habitsDog);

        Human motherFamily1 = new Human("Jessica", "Karleone", 1970);
        Human fatherFamily1 = new Human("Jack", "Karleone", 1968);

        Pet dogFamily21 = new Pet("doggie 1", Species.DOG);
        Pet dogFamily22 = new Pet("doggie 2", Species.DOG);


        Human motherFamily2 = new Human("Marta", "Connor", 1970);
        Human fatherFamily2 = new Human("Jack", "Connor", 1968);

        FamilyController fc = new FamilyController();
        fc.createNewFamily(motherFamily1, fatherFamily1);
        fc.createNewFamily(motherFamily2, fatherFamily2);

        Family family1 = fc.getFamilyById(1);
        Family family2 = fc.getFamilyById(0);

        fc.adoptChild(family1, new Man("Mike", "Tomphson", 1998));
        fc.addPet(0, dogFamily11);
        fc.addPet(0, dogFamily12);

        fc.bornChild(family2, "John", "Jessica");
        fc.bornChild(family2, "Tom", "Jessica");
        fc.bornChild(family2, "Robert", "Jessica");
        fc.bornChild(family1, "Robert", "Jessica");
        Menu comand = new Menu(fc);
        Scanner scan = new Scanner(System.in);
        while (true) {
            comand.displayCommands();
            String com = scan.next();
            if (com.toLowerCase().equals("exit"))
                break;
            else
                comand.read(Integer.parseInt(com));
        }
    }
}
