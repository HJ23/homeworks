package hw12;

import hw12.controller.FamilyController;
import hw12.classes.Human;
import hw12.exceptions.FamilyOverFlowException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import java.util.StringJoiner;

public class Menu {
    private FamilyController fc = null;
    private String commands = "\n- 1. Display the entire list of families \n" +
            "- 2. Display a list of families where the number of people is greater than the specified number\n" +
            "- 3. Display a list of families where the number of people is less than the specified number\n" +
            "- 4. Calculate the number of families where the number of members is requested\n" +
            "- 5. Create a new family\n" +
            "- 6. Delete a family by its index in the general list\n" +
            "- 7. Edit a family by its index in the general list\n" +
            "- 8. Remove all children over the age of majority (all families remove children over the age of majority - let us assume they have grown up)\n";

    private Scanner scan = new Scanner(System.in);

    public Menu(FamilyController fc) {
        this.fc = fc;
    }

    public void displayCommands() {
        System.out.println(commands);
    }

    public boolean check(int command) {
        if (command > 8 || command <= 0) {
            System.out.println("Wrong Command try again!");
            return false;
        }
        return true;
    }

    public void read(int command) throws ParseException {
        if (check(command)) {
            switch (command) {
                case 1:
                    display();
                    break;
                case 2:
                    dispGreaterGiven();
                    break;
                case 3:
                    dispLessGiven();
                    break;
                case 4:
                    dispRequested();
                    break;
                case 5:
                    Human mother = humanCreator("mother");
                    Human father = humanCreator("father");
                    fc.createNewFamily(mother, father);
                    break;
                case 6:
                    remove();
                    break;
                case 7:
                    int choice = Integer.parseInt(request("1.Give a birth \n2.Adopt a child\n3.Return to main menu"));
                    if (choice == 1)
                        giveBirth();
                    else if (choice == 2)
                        adoptChild();
                    else
                        displayCommands();
                    break;
                case 8:
                    removeChildOverAge();
                    break;

            }
        }
    }

    public void removeChildOverAge() {
        int age = Integer.parseInt(request("age :"));
        fc.deleteAllChildrenOlderThen(age);
    }

    public void adoptChild() {
        int index = Integer.parseInt(request("Family index : "));
        String name = request("name for child :");
        String surname = request("surname for child :");
        int iq = Integer.parseInt(request("surname for child :"));
        fc.adoptChild(fc.getFamilyById(index), new Human(name, surname, iq));
    }

    public void giveBirth() {
        int index = Integer.parseInt(request("Family index : "));
        String boyName = request("Boy name :");
        String girlName = request("Girl name :");
        fc.bornChild(fc.getFamilyById(index), boyName, girlName);
    }

    private String request(String query) {
        System.out.println(">>> " + query);
        return scan.next();
    }

    public void remove() {
        try {
            int index = Integer.parseInt(request("Index number:"));
            fc.deleteFamilyByIndex(index);
        } catch (FamilyOverFlowException exp) {
            exp.printStackTrace();
        }
    }

    public Human humanCreator(String query) throws ParseException {
        String name = "", surname = "";
        StringJoiner joiner = new StringJoiner("/");
        int iq = 0;
        name = request(query + " name :");
        surname = request(query + " surname :");
        joiner.add(request(query + " birthday :"));
        joiner.add(request(query + " birthmonth :"));
        joiner.add(request(query + " birthyear :"));
        iq = Integer.parseInt(request(query + " iq :"));

        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        Date date = formatter.parse(joiner.toString());
        long milliSecond = date.getTime();
        Human hum = new Human(name, surname, iq);
        hum.setBirthDate(milliSecond);
        return hum;

    }

    public void dispRequested() {
        int number = Integer.parseInt(request("number size : "));
        System.out.println(fc.countFamiliesWithMemberNumber(number));
    }

    public void dispLessGiven() {
        int number = Integer.parseInt(request("Number size : "));
        fc.getFamiliesLessThan(number).forEach(item -> System.out.println(item.prettyFormat()));
    }

    public void dispGreaterGiven() {
        int numberMember = Integer.parseInt(request("Member size : "));
        fc.getFamiliesBiggerThan(numberMember).forEach(item -> System.out.println(item.prettyFormat()));
    }

    public void display() {
        fc.displayAllFamilies();
    }
}
