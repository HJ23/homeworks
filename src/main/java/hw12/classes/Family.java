package hw12.classes;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Family {
    private Human mother = null;
    private Human father = null;
    private List<Human> children = new ArrayList<>();
    private Set<Pet> pet = new HashSet<>();

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
    }

    public String prettyFormat() {
        String temp = "family : \n";
        temp += "        mother : " + mother.prettyFormat() + "\n";
        temp += "        father : " + father.prettyFormat() + "\n";
        if (children.size() != 0) {
            temp += "children : \n";
            for (Human hum : children) {
                if (hum instanceof Man)
                    temp += "        boy :" + hum.prettyFormat() + "\n";
                else
                    temp += "        girl :" + hum.prettyFormat() + "\n";
            }
        }
        if (pet.size() != 0) {
            temp += "pets : \n      ";
            for (Pet p : pet)
                temp += p.prettyFormat();
        }
        return temp;
    }


    public void addChild(Human ch) {
        children.add(ch);
    }

    public boolean deleteChild(int index) {
        index--;
        if (index > children.size()) {
            System.out.println("Index out of Range !");
            return false;
        } else {
            children.remove(index);
            return true;
        }
    }

    public int countFamily() {
        int size = 0;
        if (mother != null)
            size++;
        if (father != null)
            size++;
        if (children != null)
            size += children.size();
        return size;
    }

    @Override
    public String toString() {
        String temp = "";
        temp += "mother : " + mother.toString() + "\n";
        temp += "father : " + father.toString() + "\n";
        if (children.size() != 0) {
            for (Human a : children)
                temp += "child :" + a.toString() + "\n";
        }
        if (pet.size() != 0) {
            for (Pet a : pet)
                temp += "pet : " + a.toString() + " \n";
        }
        return temp;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public Human getFather() {
        return this.father;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getMother() {
        return this.mother;
    }

    public void setChildren(ArrayList<Human> children) {
        this.children = children;
    }

    public List<Human> getChildren() {
        return this.children;
    }

    public void setPet(Set<Pet> pet) {
        this.pet = pet;
    }

    public Set<Pet> getPet() {
        return this.pet;
    }
}
