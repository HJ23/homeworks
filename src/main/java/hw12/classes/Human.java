package hw12.classes;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class Human {
    private String name;
    private String surname;
    private long birthDate = 0;
    private int date = 0;
    private int iq = 0;
    private Pet pet = new Pet();
    private Human mother = null;
    private Human father = null;
    private Map<String, String> schedule = new HashMap<>();
    private Family family = null;


    public Human() {
    }

    public Human(String name, String surname, int date) {
        this.name = name;
        this.surname = surname;
        this.date = date;
    }

    public Human(String name, String surname, int date, Human mother, Human father) {
        this(name, surname, date);
        this.mother = mother;
        this.father = father;
    }

    public Human(String name, String surname, int date, Human mother, Human father, int iq, Map<String, String> schedule, Pet pet) {
        this(name, surname, date, mother, father);
        this.schedule = schedule;
        this.pet = pet;
        this.iq = iq;
    }

    public String prettyFormat() {
        return ("{name=" + name + ", surname=" + surname + ", year=" + date + ", iq=" + iq + ",shcedule=" + schedule.toString() + "}");
    }


    public void describeAge() {
        long diff = System.currentTimeMillis();
        diff = (diff - date);
        long diffSeconds = diff / 1000 % 60;
        long diffMinutes = diff / (60 * 1000) % 60;
        long diffHours = diff / (60 * 60 * 1000) % 24;
        long diffDays = diff / (24 * 60 * 60 * 1000);

        System.out.print(diffDays + " days, ");
        System.out.print(diffHours + " hours, ");
        System.out.print(diffMinutes + " minutes, ");
        System.out.println(diffSeconds + " seconds.");
    }


    private long stringMilliSecondConverter(String time) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        Date date = formatter.parse(time);
        long milliSecond = date.getTime();
        return milliSecond;
    }


    public String getName() {
        return name;
    }

    public long getDate() {
        return date;
    }

    public void setName(String temp) {
        name = temp;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String temp) {
        name = temp;
    }

    public void setDate(int date) {
        this.date = date;
    }

    public void setBirthDate(long date) {
        this.birthDate = date;
    }

    public void getBirthDate(long date) {
        this.birthDate = date;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public Family getFamily() {
        return this.family;
    }


    public String greetPet() {
        printMe(("Hello," + pet.getNickname()));
        return ("Hello," + pet.getNickname());
    }

    public String describePet() {
        printMe("I have a " + pet.getNickname() + ", he is " + pet.getAge() + " years old, he is " + (pet.getTricklevel() > 50 ? "very sly" : "almost not sly"));
        return ("I have a " + pet.getNickname() + ", he is " + pet.getAge() + " years old, he is " + (pet.getTricklevel() > 50 ? "very sly" : "almost not sly"));
    }

    @Override
    public String toString() {
        return ("Human {name=" + name + ", surname=" + surname + ", year=" + date + ", iq=" + iq + ",shcedule=" + schedule.toString() + "}");
    }

    public boolean feedPet(boolean hungry) {
        if (!hungry) {
            Random temp = new Random();
            int tempint = 100 * temp.nextInt();
            if (tempint < pet.getTricklevel()) {
                printMe("Hm... I will feed Jack's " + pet.getNickname());
                return true;
            }
            printMe("I think Jack is not hungry.");
            return false;
        } else {
            printMe("Hm... I will feed Jack's " + pet.getNickname());
            return true;
        }
    }


    public static void printMe(String temp) {
        System.out.println(temp);
    }

    @Override
    public boolean equals(Object obj) {
        if (this.hashCode() == obj.hashCode()) {
            return true;
        }
        return false;
    }
}
