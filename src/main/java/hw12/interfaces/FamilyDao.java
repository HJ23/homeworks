package hw12.interfaces;

import hw12.classes.Family;
import hw12.exceptions.FamilyOverFlowException;

import java.util.List;

public interface FamilyDao {

    List<Family> getAllFamilies();
    Family getFamilyByIndex(int index);
    void deleteFamily(int index) throws FamilyOverFlowException;
    void saveFamily(Family fams);
}
