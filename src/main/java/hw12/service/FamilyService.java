package hw12.service;

import hw12.classes.*;
import hw12.collectionFamily.CollectionFamilyDAO;
import hw12.interfaces.FamilyDao;
import hw12.exceptions.FamilyOverFlowException;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

public class FamilyService {
    FamilyDao dao = new CollectionFamilyDAO();

    public List<Family> getAllFamilies() {
        return dao.getAllFamilies();
    }

    public void displayAllFamilies() {
        getAllFamilies().forEach(item -> System.out.println(item.prettyFormat()));
    }

    public List<Family> getFamiliesBiggerThan(int size) {
        List<Family> biggerthan =getAllFamilies().stream().filter(
                item -> item.countFamily()>size).collect(Collectors.toCollection(ArrayList::new));
        return biggerthan;
    }

    public List<Family> getFamiliesLessThan(int size) {
        List<Family> lessthan =getAllFamilies().stream().filter(
                item -> item.countFamily()<size).collect(Collectors.toCollection(ArrayList::new));
        return lessthan;
    }

    public int countFamiliesWithMemberNumber(int memberSize) {
        ArrayList<Family> temp = (ArrayList<Family>) getAllFamilies();
        return (int) temp.stream().filter(item -> (item.countFamily() == memberSize)).count();
    }


    public void createNewFamily(Human human1, Human human2) {
        Family temp = new Family(human1, human2);
        dao.saveFamily(temp);
    }

    public void deleteFamilyByIndex(int index) throws FamilyOverFlowException {
        dao.deleteFamily(index);
    }

    public void bornChild(Family fam, String masName, String femName) {
        if(dao.getAllFamilies().contains(fam)) {
            Random generate = new Random();
            if (generate.nextInt(100) % 2 == 0) {
                fam.addChild(new Man(masName, fam.getFather().getSurname(), (int)(generate.nextInt(20) + fam.getFather().getDate())));
            } else {
                fam.addChild(new Woman(femName, fam.getFather().getSurname(), (int)(generate.nextInt(20) + fam.getFather().getDate())));
            }
            dao.saveFamily(fam);
        }else
            throw new FamilyOverFlowException();
    }


    public void adoptChild(Family fam, Human child) {
        if (dao.getAllFamilies().contains(fam)) {
            int index = dao.getAllFamilies().indexOf(fam);
            fam.addChild(child);
            dao.saveFamily(fam);
        } else
            throw new FamilyOverFlowException();
    }


    public void deleteAllChildrenOlderThen(int age) {
        getAllFamilies().stream().forEach(
                fam -> fam.getChildren().removeIf(human -> (2019 - human.getDate()) > age));
    }


    public int count() {
        return getAllFamilies().size();
    }

    public Family getFamilyById(int index) {
        return dao.getFamilyByIndex(index);
    }

    public Set<Pet> getPets(int indexFamily) {
        return dao.getFamilyByIndex(indexFamily).getPet();
    }

    public void addPet(int index, Pet pet) {
        getAllFamilies().get(index).getPet().add(pet);
    }
}
