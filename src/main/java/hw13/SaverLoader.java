package hw13;

import hw13.classes.Family;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class SaverLoader {
    public SaverLoader(){
    }

    public void save(String path, List<Family> data) throws FileNotFoundException {
        try {
            FileOutputStream fileOut = new FileOutputStream(path);
            ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
            objectOut.writeObject(data);
            objectOut.close();
            fileOut.close();
            System.out.println("The Object  was succesfully written to a file");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public List<Family> load(String path) throws IOException, ClassNotFoundException {

        FileInputStream fis = new FileInputStream(path);
        List<Family> objectsList = new ArrayList<Family>();
        boolean cont = true;
        ObjectInputStream input = new ObjectInputStream(fis);
        objectsList = (List<Family>) input.readObject();
        input.close();
        fis.close();
        return objectsList;
    }
}
