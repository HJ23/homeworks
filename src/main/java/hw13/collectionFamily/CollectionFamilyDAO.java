package hw13.collectionFamily;

import hw13.classes.Family;
import hw13.interfaces.FamilyDao;
import hw13.exceptions.FamilyOverFlowException;

import java.util.ArrayList;
import java.util.List;

public class CollectionFamilyDAO implements FamilyDao {
    private List<Family> families = new ArrayList<>();


    @Override
    public List<Family> getAllFamilies() {
        return families;
    }

    @Override
    public Family getFamilyByIndex(int index) {
        if (index > families.size())
            return null;
        return families.get(index);
    }

    @Override
    public void deleteFamily(int index) throws FamilyOverFlowException {
        if (index > families.size())
            throw new FamilyOverFlowException();
        else
        families.remove(index);
    }

    @Override
    public void saveFamily(Family fam) {
        if (families.contains(fam))
            families.set(families.indexOf(fam), fam);
        else
            families.add(fam);
    }

    @Override
    public void loadData(List<Family> data) {
        families=data;
    }
}
