package hw13.controller;

import hw13.classes.Family;
import hw13.classes.Human;
import hw13.classes.Pet;
import hw13.service.FamilyService;
import hw13.exceptions.FamilyOverFlowException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class FamilyController {
    FamilyService dao = new FamilyService();

    public void loadData(List<Family> data) {
        dao.loadData(data);
    }

    public void saveData() throws FileNotFoundException {
        dao.saveData("abc.ser");
    }

    public void loadFromMemory() throws IOException, ClassNotFoundException {
        dao.loadFromMemory();
    }

    public List<Family> getAllFamilies() {
        return dao.getAllFamilies();
    }

    public void displayAllFamilies() {
        dao.displayAllFamilies();
    }

    public List<Family> getFamiliesBiggerThan(int size) {
        return dao.getFamiliesBiggerThan(size);
    }

    public List<Family> getFamiliesLessThan(int size) {
        return dao.getFamiliesLessThan(size);
    }

    public int countFamiliesWithMemberNumber(int memberSize) {
        return dao.countFamiliesWithMemberNumber(memberSize);
    }

    public void createNewFamily(Human human1, Human human2) {
        dao.createNewFamily(human1, human2);
    }

    public void deleteFamilyByIndex(int index) throws FamilyOverFlowException {
        dao.deleteFamilyByIndex(index);
    }

    public void bornChild(Family fam, String masName, String femName) {
        dao.bornChild(fam, masName, femName);
    }

    public void adoptChild(Family fam, Human child) {
        dao.adoptChild(fam, child);
    }

    public void deleteAllChildrenOlderThen(int age) {
        dao.deleteAllChildrenOlderThen(age);
    }

    public int count() {
        return dao.count();
    }

    public Family getFamilyById(int index) {
        return dao.getFamilyById(index);
    }

    public Set<Pet> getPets(int indexFamily) {
        return dao.getPets(indexFamily);
    }

    public void addPet(int index, Pet pet) {
        dao.addPet(index, pet);
    }


}
