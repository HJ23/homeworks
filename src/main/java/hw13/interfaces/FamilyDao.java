package hw13.interfaces;

import hw13.classes.Family;
import hw13.exceptions.FamilyOverFlowException;

import java.util.List;

public interface FamilyDao {

    List<Family> getAllFamilies();
    Family getFamilyByIndex(int index);
    void deleteFamily(int index) throws FamilyOverFlowException;
    void saveFamily(Family fams);
    void loadData(List<Family> data);
}
