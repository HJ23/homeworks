package hw2;

import java.util.Random;
import java.util.Scanner;

public class Arrays {
    static String[][] matrix = new String[][]{{"-", "-", "-", "-", "-"}, {"-", "-", "-", "-", "-"}
            , {"-", "-", "-", "-", "-"}, {"-", "-", "-", "-", "-"}, {"-", "-", "-", "-", "-"}};

    public static void print() {
        System.out.println("0 | 1 | 2 | 3 | 4 | 5 |");
        for (int x = 0; x < 5; x++) {
            System.out.print(x + 1);
            for (int y = 0; y < 5; y++)
                System.out.print(" | " + matrix[x][y]);
            System.out.println(" | ");
        }
    }

    public static void reset() {
        for (int x = 0; x < 5; x++) {
            for (int y = 0; y < 5; y++) {
                matrix[x][y] = "-";
            }
        }
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Random randomGenerator = new Random();
        int enemyRow = randomGenerator.nextInt(5);
        int enemyColumn = randomGenerator.nextInt(5);
        System.out.println("All set. Get ready to rumble!");
        while (true) {
            System.out.println("enter the row number :");
            int userRow = scan.nextInt();
            System.out.println("enter the column number :");
            int userColumn = scan.nextInt();
            if (userRow > 5 || userRow < 0 || enemyColumn > 5 || enemyColumn < 0) ;
            else {
                if (userColumn == enemyColumn && userRow == enemyRow) {
                    matrix[userRow - 1][userColumn - 1] = "x";
                    print();
                    System.out.println("You have won!");
                    break;
                } else {
                    matrix[userRow - 1][userColumn - 1] = "*";
                    print();
                }
            }
        }
    }
}