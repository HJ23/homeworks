package hw3;

import java.util.Scanner;

public class Schedule {
    public static void main(String[] args) {
        String[][] schedule = new String[7][2];
        schedule[0][0] = "Sunday";
        schedule[0][1] = "do home work";
        schedule[1][0] = "Monday";
        schedule[1][1] = "go to courses; watch a film";
        schedule[2][0] = "Tuesday";
        schedule[2][1] = "go to school";
        schedule[3][0] = "Wednesday";
        schedule[3][1] = "Watch movie";
        schedule[4][0] = "Thursday";
        schedule[4][1] = "Make meeting";
        schedule[5][0] = "Friday";
        schedule[5][1] = "prepare bread";
        schedule[6][0] = "Saturday";
        schedule[6][1] = "meeting with members";

        Scanner scan = new Scanner(System.in);
        boolean exit = false;
        while (true) {
            System.out.println("Please, input the day of the week:");
            String scanned = scan.nextLine();
            scanned = scanned.toLowerCase();
            scanned = scanned.replace(" ", "");
            switch (scanned) {
                case "monday":
                    System.out.println("Your tasks for " + scanned + ": " + schedule[1][1]);
                    break;
                case "sunday":
                    System.out.println("Your tasks for " + scanned + ": " + schedule[0][1]);
                    break;
                case "tuesday":
                    System.out.println("Your tasks for " + scanned + ": " + schedule[2][1]);
                    break;
                case "wednesday":
                    System.out.println("Your tasks for " + scanned + ": " + schedule[3][1]);
                    break;
                case "thursday":
                    System.out.println("Your tasks for " + scanned + ": " + schedule[4][1]);
                    break;
                case "friday":
                    System.out.println("Your tasks for " + scanned + ": " + schedule[5][1]);
                    break;
                case "saturday":
                    System.out.println("Your tasks for " + scanned + ": " + schedule[6][1]);
                    break;
                case "exit":
                    exit = true;
                    break;
                default:
                    System.out.println("Sorry, I don't understand you, please try again.");
            }
            if (exit)
                break;
        }
    }
}
