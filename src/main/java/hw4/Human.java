package hw4;

import java.util.Random;

public class Human {
    public String name;
    public String surname;
    public int date;
    public int iq;
    public Pet pet = new Pet();
    public Human mother = null;
    public Human father = null;
    public int[][] schedule;

    public Human() {
    }

    public Human(String name, String surname, int date) {
        this.name = name;
        this.surname = surname;
        this.date = date;
    }

    public Human(String name, String surname, int date, Human mother, Human father) {
        this(name, surname, date);
        this.mother = mother;
        this.father = father;
    }

    public Human(String name, String surname, int date, Human mother, Human father, int iq, int[][] schedule, Pet pet) {
        this(name, surname, date, mother, father);
        this.schedule = schedule;
        this.pet = pet;
        this.iq = iq;
    }

    public String greetPet() {
        printme(("Hello," + pet.nickName));
        return ("Hello," + pet.nickName);
    }

    public String describePet() {
        printme("I have a " + pet.nickName + ", he is " + pet.age + " years old, he is " + (pet.trickLevel > 50 ? "very sly" : "almost not sly"));
        return ("I have a " + pet.nickName + ", he is " + pet.age + " years old, he is " + (pet.trickLevel > 50 ? "very sly" : "almost not sly"));
    }

    @Override
    public String toString() {
        String father_name = "underfined", mother_name = "undefined";
        if (father != null)
            father_name = father.name;
        if (mother != null)
            mother_name = mother.name;
        printme(("Human{name=" + name + ", surname=" + surname + ", year=" + date + ", iq=" + iq + ", mother=" + mother_name + ", father=" + father_name + "," + pet.toString()));
        return ("Human{name=" + name + ", surname=" + surname + ", year=" + date + ", iq=" + iq + ", mother=" + mother_name + ", father=" + father_name + "," + pet.toString());
    }

    public boolean feedPet(boolean hungry) {
        if (!hungry) {  //not hungry
            Random temp = new Random();
            int tempint = 100 * temp.nextInt();
            if (tempint < pet.trickLevel) {
                printme("Hm... I will feed Jack's " + pet.nickName);
                return true;
            }
            printme("I think Jack is not hungry.");
            return false;
        } else {
            printme("Hm... I will feed Jack's " + pet.nickName);
            return true;
        }
    }

    public static void printme(String temp) {
        System.out.println(temp);
    }
}