package hw4;

public class Main {
    public static void main(String[] args) {
        int[][] tempSchedule = new int[7][7];
        Pet pet1 = new Pet("Rock", "Wolf", 5, 75, new String[]{"eat", "drink", "sleep"});
        Human temp = new Human();
        Human mother = new Human("Jane", "Karleone", 1950);
        Human father = new Human("Vito", "Karleone", 1950);
        Human son2 = new Human("Michael", "Karleone", 1977, mother, father, 90, tempSchedule, pet1);
        son2.toString();
        son2.feedPet(true);
        son2.describePet();
        son2.greetPet();
    }
}
