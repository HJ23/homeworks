package hw4;

import java.util.Arrays;

public class Pet {
    public String species = "unknown";
    public String nickName = "unknown";
    public int age = -1;
    public int trickLevel = -1;
    public String[] habits;

    public Pet(String name, String species) {
        this.nickName = name;
        this.species = species;
    }

    public Pet(String name, String species, int age, int tricklevel, String[] habits) {
        this.nickName = name;
        this.species = species;
        this.age = age;
        this.trickLevel = tricklevel;
        this.habits = habits;

    }

    public Pet() {

    }

    public void eat() {
        System.out.println("I am eating");
    }

    public void respond() {
        System.out.println("Hello, owner. I am - " + nickName + ". I miss you!");
    }

    public void foul() {
        System.out.println("I need to cover it up");
    }

    @Override
    public String toString() {
        return "pet=dog{nickname=" + nickName + ", age=" + age + ", trickLevel=" + trickLevel + ", habits=" + Arrays.toString(habits) + "}}";
    }
}
