package hw5;

import java.util.ArrayList;

public class Family {

    public Human mother = null;
    public Human father = null;
    public ArrayList<Human> children = new ArrayList<>();
    public Pet pet = null;

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
    }

    public void addChild(Human ch) {
        children.add(ch);
    }


    public boolean deleteChild(Human child) {
        if (children.contains(child)) {
            children.remove(child);
            return true;
        }
        return false;

    }


    public boolean deleteChild(long index) {
        if (index >= children.size()) {
            System.out.println("Index out of Range !");
            return false;
        } else {
            children.remove((int) index);
            return true;
        }
    }

    public long countFamily() {
        long size = 0;

        if (mother != null)
            size++;
        if (father != null)
            size++;
        if (children != null)
            size += children.size();
        return size;
    }

    @Override
    public String toString() {
        String temp = "";
        temp += "mother : " + mother.toString() + "\n";
        temp += "father : " + father.toString() + "\n";
        if (children.size() != 0) {
            for (Human a : children)
                temp += "child :" + a.toString() + "\n";
        }
        return temp;
    }


    public void setFather(Human father) {
        this.father = father;
    }

    public Human getFather() {
        return this.father;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getMother() {
        return this.mother;
    }

    public void setChildren(ArrayList<Human> children) {
        this.children = children;
    }

    public ArrayList<Human> getChildren() {
        return this.children;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public Pet getPet() {
        return this.pet;
    }

}
