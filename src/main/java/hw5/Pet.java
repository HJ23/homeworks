package hw5;

import java.util.Arrays;

public class Pet {
    private String species = "unknown";
    private String nickName = "unknown";
    private int age = -1;
    private int trickLevel = -1;
    private String[] habits;

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String temp) {
        this.species = temp;
    }

    public String getNickname() {
        return nickName;
    }

    public void setNickname(String temp) {
        this.nickName = temp;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int temp) {
        this.age = temp;
    }

    public int getTricklevel() {
        return trickLevel;
    }

    public void setTricklevel(int temp) {
        this.trickLevel = temp;
    }

    public String[] getHabits() {
        return habits;
    }

    public void setHabits(String[] temp) {
        this.habits = temp;
    }

    public Pet(String name, String species) {
        this.nickName = name;
        this.species = species;
    }

    public Pet(String name, String species, int age, int tricklevel, String[] habits) {
        this(name, species);
        this.age = age;
        this.trickLevel = tricklevel;
        this.habits = habits;
    }

    public Pet() {

    }

    public void eat() {
        System.out.println("I am eating");
    }

    public void respond() {
        System.out.println("Hello, owner. I am - " + nickName + ". I miss you!");
    }

    public void foul() {
        System.out.println("I need to cover it up");
    }

    @Override
    public String toString() {
        return "pet=dog{nickname=" + nickName + ", age=" + age + ", trickLevel=" + trickLevel + ", habits=" + Arrays.toString(habits) + "}}";
    }
}
