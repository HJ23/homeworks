package hw6;

public class Family {
    public Human mother = null;
    public Human father = null;
    public Human[] children = new Human[100];
    private int childCount = 0;
    public Pet pet = null;

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
    }

    public void addChild(Human child) {
        children[childCount] = child;
        childCount++;
    }

    public boolean deleteChild(int index) {
        if (index >= (childCount + 1)) {
            System.out.println("Index out of Range !");
            return false;
        } else {
            children[index] = null;
            return true;
        }
    }

    public Human deleteChild(Human child) {

        for (int x=0;x<children.length ;x++) {
            if (children[x].hashCode() == child.hashCode()) {
                if (children[x].equals(child)) {
                    Human tempChild=children[x];
                    children[x] = null;
                    childCount--;
                    return tempChild;
                }
            }
        }
        return null;
    }

    public int countFamily() {
        int size = 0;
        if (mother != null)
            size++;
        if (father != null)
            size++;
        if (children != null)
            size += childCount;
        return size;
    }

    @Override
    public String toString() {
        String temp = "";
        temp += "mother : " + mother.toString() + "\n";
        temp += "father : " + father.toString() + "\n";
        if (childCount!= 0) {
            for (Human child : children)
                if(child!=null)
                   temp += "child :" + child.toString() + "\n";
        }
        return temp;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public Human getFather() {
        return this.father;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getMother() {
        return this.mother;
    }

    public void setChildren(Human[] children) {
        this.children = children;
    }

    public Human[] getChildren() {
        return this.children;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public Pet getPet() {
        return this.pet;
    }

    @Override
    protected void finalize() throws Throwable {

        System.out.println("Family object deconstructed!");
        super.finalize();
    }
}