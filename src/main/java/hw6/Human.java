package hw6;

import java.util.Random;

public class Human {

    private String name;
    private String surname;
    private int date = 0;
    private int iq = 0;
    private Pet pet = null;
    private Human mother = null;
    private Human father = null;
    private String[][] schedule = {{"", ""}};
    private Family family;


    private String getSchedule() {
        String str = "";
        for (int x = 0; x < schedule.length; x++) {
            str += " [ " + schedule[x][0] + "  " + schedule[x][1] + "] ";
        }
        return str;
    }


    public Human() {

    }

    public Human(String name, String surname, int date) {
        this.name = name;
        this.surname = surname;
        this.date = date;

    }

    public Human(String name, String surname, int date, Human mother, Human father) {
        this(name, surname, date);
        this.mother = mother;
        this.father = father;

    }

    public Human(String name, String surname, int date, Human mother, Human father, int iq, String[][] schedule, Pet pet) {
        this(name, surname, date, mother, father);
        this.schedule = schedule;
        this.pet = pet;
        this.iq = iq;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public Family getFamily() {
        return this.family;
    }

    public String greetPet() {
        printMe(("Hello," + pet.getNickname()));
        return ("Hello," + pet.getNickname());
    }

    public String describePet() {
        printMe("I have a " + pet.getNickname() + ", he is " + pet.getAge() + " years old, he is " + (pet.getTricklevel() > 50 ? "very sly" : "almost not sly"));
        return ("I have a " + pet.getNickname() + ", he is " + pet.getAge() + " years old, he is " + (pet.getTricklevel() > 50 ? "very sly" : "almost not sly"));
    }

    @Override
    public String toString() {
        printMe(("Human{name=" + name + ", surname=" + surname + ", year=" + date + ", iq=" + iq + ",shcedule=" + getSchedule()));
        return ("Human{name=" + name + ", surname=" + surname + ", year=" + date + ", iq=" + iq + ",shcedule=" + getSchedule());
    }

    public boolean feedPet(boolean hungry) {
        if (!hungry) {
            Random randomGenerator = new Random();
            int trickLevel = 100 * randomGenerator.nextInt();

            if (trickLevel < pet.getTricklevel()) {
                printMe("Hm... I will feed Jack's " + pet.getNickname());
                return true;
            }
            printMe("I think Jack is not hungry.");
            return false;
        } else {
            printMe("Hm... I will feed Jack's " + pet.getNickname());
            return true;
        }

    }

    public static void printMe(String temp) {
        System.out.println(temp);
    }

    @Override
    public boolean equals(Object obj) {

        if (this.hashCode() == obj.hashCode()) {
            return true;
        }
        return false;
    }

    @Override
    protected void finalize() throws Throwable {

        System.out.println("Human object deconstructed!");
        super.finalize();
    }
}