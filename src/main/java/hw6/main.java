package hw6;

public class main {
    public static void main(String[] args) {
        String[][] schedule = {{DayofWeek.MONDAY.name(), "play football"},
                {DayofWeek.TUESDAY.name(), "play voleyball"},
                {DayofWeek.WEDNESDAY.name(), "goto school"},
                {DayofWeek.THURSDAY.name(), "buy bike"},
                {DayofWeek.FRIDAY.name(), "buy car"},
                {DayofWeek.SATURDAY.name(), "make food"},
                {DayofWeek.SUNDAY.name(), "goto college"}
        };
        Pet pet_ = new Pet("doggie", Species.DOG);
        Human father = new Human("john", "karleone", 1970);
        Human mother = new Human("jessica", "karleone", 1970);
        Human son = new Human("johnson junior", "karleone", 1990, mother, father, 190, schedule, pet_);
        Family fam = new Family(mother, father);
        fam.addChild(son);
        fam.toString();

        Human[] temp = new Human[10000];
        for (Human a : temp)
            a = new Human();
        for (Human a : temp)
            a = null;
        System.gc();

    }
}
