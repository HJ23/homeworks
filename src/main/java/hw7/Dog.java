package hw7;

public class Dog extends Pet {
    public Dog() {
        super();
        this.animalSpecie = species.DOG;
    }

    @Override
    public void respond() {
        System.out.println("i am dog");
    }

    public void foul() {
        System.out.println("i can foul");
    }
}
