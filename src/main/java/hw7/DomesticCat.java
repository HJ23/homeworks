package hw7;

public class DomesticCat extends Pet {
    public DomesticCat() {
        super();
        this.animalSpecie = species.DOMESTIC_CAT;
    }

    @Override
    public void respond() {
        System.out.println("i am cat");
    }

    public void foul() {
        System.out.println("i can foul");
    }
}
