package hw7;

public class Fish extends Pet {
    public Fish() {
        super();
        this.animalSpecie = species.FISH;
    }

    public void foul() {
        System.out.println("foul function fish can");
    }

    @Override
    public void respond() {
        System.out.println("i am fish");
    }
}
