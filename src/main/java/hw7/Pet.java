package hw7;

public abstract class Pet {
    enum species {FISH, DOMESTIC_CAT, ROBO_CAT, DOG, UNKNOWN}

    protected species animalSpecie = species.UNKNOWN;

    public Pet() {
    }

    public void run() {
        System.out.println(animalSpecie);
    }

    public void eat() {
        System.out.println("animal eats something!");
    }

    public abstract void respond();
}
