package hw7;

public class RoboCat extends Pet {
    public RoboCat() {
        super();
        this.animalSpecie = species.ROBO_CAT;
    }

    @Override
    public void respond() {
        System.out.println("i am robocat");
    }

    public void foul() {
        System.out.println("i can foul");
    }
}
