package hw7;

public class Woman extends Human {
    @Override
    public void greetPet() {
        System.out.println("GreetPet from woman class!");
    }

    public void makeup() {
        System.out.println("i am doing makeup");
    }
}
