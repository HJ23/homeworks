package hw7;

public class main {
    public static void main(String[] args) {
        Pet animal = new Dog();
        animal.respond();
        System.out.println("My specie is : " + animal.animalSpecie);
        animal = new DomesticCat();
        animal.respond();
        System.out.println("My specie is : " + animal.animalSpecie);
        animal = new RoboCat();
        animal.respond();
        System.out.println("My specie is : " + animal.animalSpecie);
        animal = new Fish();
        animal.respond();
        System.out.println("My specie is : " + animal.animalSpecie);

        Human human = new Man();
        human.greetPet();
        human = new Woman();
        human.greetPet();

        Man man = new Man();
        man.repairCar();
        Woman woman = new Woman();
        woman.makeup();


    }
}
