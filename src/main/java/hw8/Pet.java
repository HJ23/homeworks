package hw8;

import java.util.Set;

public class Pet {
    private Species species = Species.UNKNOWN;
    private String nickname = "unknown";
    private int age = -1;
    private int trickLevel = -1;
    private Set<String> habits = null;

    public Species getSpecies() {
        return species;
    }

    public void setSpecies(Species temp) {
        this.species = temp;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String temp) {
        this.nickname = temp;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int temp) {
        this.age = temp;
    }

    public int getTricklevel() {
        return trickLevel;
    }

    public void setTricklevel(int temp) {
        this.trickLevel = temp;
    }

    public Set<String> getHabits() {
        return habits;
    }

    public void setHabits(Set<String> temp) {
        this.habits = temp;
    }

    public Pet(String name, Species species) {
        this.nickname = name;
        this.species = species;
    }

    public Pet(String name, Species species, int age, int tricklevel, Set<String> habits) {
        this(name, species);
        this.age = age;
        this.trickLevel = tricklevel;
        this.habits = habits;
    }

    public Pet() {
    }

    public void eat() {
        System.out.println("I am eating");
    }

    public void respond() {
        System.out.println("Hello, owner. I am - " + nickname + ". I miss you!");
    }

    public void foul() {
        System.out.println("I need to cover it up");
    }

    @Override
    public String toString() {
        return "pet=dog{nickname=" + nickname + ", age=" + age + ", trickLevel=" + trickLevel + ", habits=" + habits.toString() + "}}";
    }
}