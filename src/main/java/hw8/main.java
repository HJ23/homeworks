package hw8;

import java.util.*;

public class main {
    public static void main(String[] args) {
        Set<String> habits = new HashSet<>();
        habits.add("eating");
        habits.add("playing");
        habits.add("sleeping");
        Map<Integer, String> schedule = new HashMap<>();
        schedule.put(1, "Monday: get to work");
        schedule.put(2, "Tuesday: meeting in the park");
        schedule.put(3, "Wednesday: walk with dog");
        schedule.put(4, "Thursday: make bread with josh");
        Pet p1 = new Pet("jack", Species.DOG, 6, 70, habits);
        Pet p2 = new Pet("john", Species.DOG, 6, 70, habits);
        Set<Pet> pets = new HashSet<>();
        pets.add(p1);
        pets.add(p2);
        Human mother = new Human("jessica", "Karleone", 1970);
        Human father = new Human("johnson", "Karleone", 1970);
        Human son = new Human("johnson junior ", "Karleone", 1990, mother, father, 190, schedule, p1);
        Family fam = new Family(mother, father);
        fam.addChild(son);
        fam.setPet(pets);
        fam.toString();
    }
}
