package hw9.Dao;

import hw9.classes.Family;
import hw9.Dao.FamilyDAO;

import java.util.ArrayList;
import java.util.List;

public class CollectionFamilyDAO implements FamilyDAO {
    private List<Family> families = new ArrayList<>();

    @Override
    public List<Family> getAllFamilies() {
        return families;
    }

    @Override
    public Family getFamilyByIndex(int index) {
        if (index > families.size())
            return null;
        return families.get(index);
    }

    @Override
    public boolean deleteFamily(int index) {
        if (index > families.size())
            return false;
        families.remove(index);
        return true;
    }

    @Override
    public void saveFamily(Family fam) {
        if (families.contains(fam))
            families.set(families.indexOf(fam), fam);
        else
            families.add(fam);
    }
}
