package hw9.Dao;

import hw9.classes.Family;

import java.util.List;

public interface FamilyDAO{

    List<Family> getAllFamilies();
    Family getFamilyByIndex(int index);
    boolean deleteFamily(int index);
    void saveFamily(Family fams );
}
