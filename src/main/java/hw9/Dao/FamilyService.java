package hw9.Dao;

import hw9.Dao.CollectionFamilyDAO;
import hw9.classes.Family;
import hw9.classes.Human;
import hw9.Dao.FamilyDAO;
import hw9.classes.Man;
import hw9.classes.Pet;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;

public class FamilyService {
    FamilyDAO dao = new CollectionFamilyDAO();

    public List<Family> getAllFamilies() {
        return dao.getAllFamilies();
    }

    public void displayAllFamilies() {
        getAllFamilies().forEach(item -> System.out.println(item.toString()));
    }

    public List<Family> getFamiliesBiggerThan(int size) {
        List<Family> biggerthan = new ArrayList<>();
        getAllFamilies().forEach(
                item -> {
                    if (item.countFamily() >= size) {
                        biggerthan.add(item);
                    }
                });
        return biggerthan;
    }

    public List<Family> getFamiliesLessThan(int size) {
        List<Family> lessthan = new ArrayList<>();
        getAllFamilies().forEach(
                item -> {
                    if (item.countFamily() < size) {
                        System.out.println(item.toString());
                        lessthan.add(item);
                    }
                });
        return lessthan;
    }

    public int countFamiliesWithMemberNumber(int memberSize) {
        int count = 0;
        List<Family> temp = getAllFamilies();
        for (Family var : temp) {
            if (var.countFamily() == memberSize)
                count++;
        }
        return count;
    }


    public void createNewFamily(Human human1, Human human2) {
        Family temp = new Family(human1, human2);
        dao.saveFamily(temp);
    }

    public void deleteFamilyByIndex(int index) {
        dao.deleteFamily(index);
    }

    public void bornChild(Family fam, String masName, String femName) {
        Random generate = new Random();
        if (generate.nextInt(100) % 2 == 0) {
            fam.addChild(new Man(masName, fam.getFather().getSurname(), generate.nextInt(20) + fam.getFather().getDate()));
        } else {
            fam.addChild(new Man(femName, fam.getFather().getSurname(), generate.nextInt(20) + fam.getFather().getDate()));
        }
        dao.saveFamily(fam);
    }


    public void adoptChild(Family fam, Human child) {
        int index = dao.getAllFamilies().indexOf(fam);
        fam.addChild(child);
        dao.saveFamily(fam);
    }


    public void deleteAllChildrenOlderThen(int age) {
        for (Family fam : getAllFamilies()) {
            fam.getChildren().removeIf(human -> (2019 - human.getDate()) > age);
            dao.saveFamily(fam);
        }
    }


    public int count() {
        return getAllFamilies().size();
    }

    public Family getFamilyById(int index) {
        return dao.getFamilyByIndex(index);
    }

    public Set<Pet> getPets(int indexFamily) {
        return dao.getFamilyByIndex(indexFamily).getPet();
    }

    public void addPet(int index, Pet pet) {
       getAllFamilies().get(index).getPet().add(pet);
    }


}
