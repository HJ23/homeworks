package hw9;


import hw9.classes.Family;
import hw9.classes.Human;
import hw9.classes.Pet;
import hw9.classes.Species;
import hw9.Dao.FamilyController;

import java.util.HashSet;
import java.util.Set;

public class main {

    public static void main(String[] args) {
        Set<String> habitsDog=new HashSet<String>();
        habitsDog.add("sleeping");
        habitsDog.add("playing");
        habitsDog.add("eating");
        habitsDog.add("running");

        Pet dogFamily11 = new Pet("doggie 1",Species.DOG,4,70,habitsDog);
        Pet dogFamily12 = new Pet("doggie 2",Species.DOG,7,70,habitsDog);

        Human motherFamily1 = new Human("Jessica", "Karleone", 1970);
        Human fatherFamily1 = new Human("Jack", "Karleone", 1968);

        Pet dogFamily21 = new Pet("doggie 1",Species.DOG);
        Pet dogFamily22 = new Pet("doggie 2",Species.DOG);



        Human motherFamily2 = new Human("Marta", "Connor", 1970);
        Human fatherFamily2 = new Human("Jack", "Connor", 1968);

        FamilyController fc = new FamilyController();
        fc.createNewFamily(motherFamily1, fatherFamily1);
        fc.createNewFamily(motherFamily2, fatherFamily2);

        Family family1 = fc.getFamilyById(1);
        Family family2 = fc.getFamilyById(0);

        fc.adoptChild(family1,new Human("Mike","Tomphson",1998));
        fc.addPet(0,dogFamily11);
        fc.addPet(0,dogFamily12);

        fc.bornChild(family2, "John", "Jessica");
        fc.bornChild(family2, "Tom", "Jessica");
        fc.bornChild(family2, "Robert", "Jessica");
        fc.bornChild(family1, "Robert", "Jessica");
        fc.displayAllFamilies();


    }


}
