package hw8;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;


public class FamilyTest {
    public Family fam;
    Human mother,father,son;
    Pet pet;
    @Before
    public void init(){
        pet = new Pet("doggie", Species.DOG);
        father = new Human("john", "karleone", 1970);
        mother = new Human("jessica", "karleone", 1970);
        son = new Human("johnson junior", "karleone", 1990, mother, father);
        fam = new Family(mother, father);
        fam.addChild(son);
    }

    @Test
    public void addChild() {
        int childCount=fam.countFamily();
        fam.addChild(son);
        assertEquals(childCount+1,fam.countFamily());
    }

    @Test
    public void deleteChild() {
        int childCount=fam.countFamily();
        fam.deleteChild(1);
        assertEquals(childCount-1,fam.countFamily());
    }


    @Test
    public void countFamily() {
        fam.addChild(son);
        assertEquals(fam.countFamily(),4);
    }

    @Test
    public void getFather() {
        assertEquals(fam.getFather(),father);
    }

    @Test
    public void getMother() {
        assertEquals(fam.getMother(),mother);
    }
}