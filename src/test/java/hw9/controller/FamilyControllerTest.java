package hw9.controller;

import hw9.Dao.FamilyController;
import hw9.classes.Family;
import hw9.classes.Human;
import hw9.classes.Pet;
import hw9.classes.Species;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;


public class FamilyControllerTest {


    FamilyController fc;

    @Before
    public void run() {
        Set<String> habitsDog = new HashSet<String>();
        habitsDog.add("sleeping");
        habitsDog.add("playing");
        habitsDog.add("eating");
        habitsDog.add("running");

        Pet dogFamily11 = new Pet("doggie 1", Species.DOG, 4, 70, habitsDog);
        Pet dogFamily12 = new Pet("doggie 2", Species.DOG, 7, 70, habitsDog);

        Human motherFamily1 = new Human("Jessica", "Karleone", 1970);
        Human fatherFamily1 = new Human("Jack", "Karleone", 1968);

        Human motherFamily2 = new Human("Marta", "Connor", 1970);
        Human fatherFamily2 = new Human("Jack", "Connor", 1968);

        fc = new FamilyController();
        fc.createNewFamily(motherFamily1, fatherFamily1);
        fc.createNewFamily(motherFamily2, fatherFamily2);

        Family family1 = fc.getFamilyById(1);
        Family family2 = fc.getFamilyById(0);

        fc.adoptChild(family1, new Human("Mike", "Tomphson", 1998));
        fc.addPet(0, dogFamily11);
        fc.addPet(0, dogFamily12);

        fc.bornChild(family2, "John", "Jessica");
        fc.bornChild(family2, "Tom", "Jessica");
        fc.bornChild(family2, "Robert", "Jessica");
        fc.bornChild(family1, "Robert", "Jessica");

    }

    @Test
    public void getAllFamilies() {
        assertEquals(fc.getAllFamilies().size(), 2);
    }

    @Test
    public void getFamiliesBiggerThan() {
        assertEquals(fc.getFamiliesBiggerThan(4).size(), 2);
    }

    @Test
    public void getFamiliesLessThan() {
        assertEquals(fc.getFamiliesBiggerThan(5).size(), 1);
    }

    @Test
    public void countFamiliesWithMemberNumber() {
        assertEquals(fc.countFamiliesWithMemberNumber(5), 1);
    }

    @Test
    public void createNewFamily() {
        fc.createNewFamily(new Human(), new Human());
        assertEquals(fc.getAllFamilies().size(), 3);
    }

    @Test
    public void deleteFamilyByIndex() {
        fc.deleteFamilyByIndex(1);
        assertEquals(fc.getAllFamilies().size(), 1);
    }

    @Test
    public void bornChild() {
        fc.createNewFamily(new Human(), new Human());
        Family fam = fc.getFamilyById(2);
        fc.bornChild(fam, "test1", "test2");
        assertEquals(fc.getFamilyById(2).getChildren().size(), 1);


    }

    @Test
    public void adoptChild() {

        fc.createNewFamily(new Human(), new Human());
        Family fam = fc.getFamilyById(2);
        fc.adoptChild(fam,new Human());
        assertEquals(fc.getFamilyById(2).getChildren().size(), 1);
    }

    @Test
    public void deleteAllChildrenOlderThen() {

        Family temp=fc.getFamilyById(0);
        fc.deleteAllChildrenOlderThen(10);
        assertEquals(temp.countFamily(), 2);
    }

    @Test
    public void count() {
        assertEquals(fc.count(),2);
    }

}
